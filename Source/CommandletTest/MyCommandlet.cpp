// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCommandlet.h"

#include "AssetRegistryModule.h"
#include "Developer/AssetTools/Public/AssetToolsModule.h"

#include "MyActor.h"

int32 UMyCommandlet::Main(const FString& Params)
{
	UE_LOG(LogTemp, Display, TEXT("================ running MyCommandlet ================"));

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	TArray<FAssetData> AssetData;
	AssetRegistryModule.Get().GetAssetsByClass(UWorld::StaticClass()->GetFName(), AssetData);
	
	for (FAssetData Asset : AssetData)
	{
		UE_LOG(LogTemp, Display, TEXT("AssetName = %s"), *Asset.AssetName.ToString());
		UE_LOG(LogTemp, Display, TEXT("PackageName = %s"), *Asset.PackageName.ToString());

		if (Asset.AssetName == FName(TEXT("FooLevel")))
		{
			UE_LOG(LogTemp, Display, TEXT("FooLevel found"));

			if (UWorld* World = Cast<UWorld>(Asset.GetAsset()))
			{
				// load world, not sure if this is entirely correct
				FWorldContext& WorldContext = GEngine->CreateNewWorldContext(World->WorldType);
				WorldContext.SetCurrentWorld(World);
				World->AddToRoot();
				GEngine->WorldAdded(World);
				if (!World->bIsWorldInitialized)
				{
					World->InitWorld(UWorld::InitializationValues().AllowAudioPlayback(false));
				}
				World->GetWorldSettings()->PostEditChange();
				World->UpdateWorldComponents(true, false);

				// find our actor in the level
				for (ULevel* Level : World->GetLevels())
				{
					for (AActor* Actor : Level->Actors)
					{
						if (AMyActor* MyActor = Cast<AMyActor>(Actor))
						{
							UE_LOG(LogTemp, Display, TEXT("MyActor found"));
							UE_LOG(LogTemp, Display, TEXT("calling BlueprintImplementableEvent Foobar..."));

							// no output
							MyActor->Foobar();

							break;
						}
					}
				}

				break;
			}
		}
	}

	UE_LOG(LogTemp, Display, TEXT("================================================================"));

	return 0;
}